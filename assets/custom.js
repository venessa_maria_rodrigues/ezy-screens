$(document).ready(function(){
  if($('[data-template]').length > 0){
    if($('[data-template]').data('template') === 'cart'){
      $('[data-remove-addon]').click(function(e){
        e.preventDefault();
        let ranNo = $(this).data('random-no');
        let sameEle = $('[data-random-no="'+ranNo+'"]');
        const key = $(this).data('key');
        var data = "updates["+key+"]=0";
        updateMainandAddon(data,ranNo);
      
      })
      
      $('.data-decrease').click(function(){
        
       const oldQty = $(this).siblings(".cart-item-quantity-display").val();
       const newQuantity = oldQty <= 1 ? 0 : oldQty - 1;
       $(this).siblings(".cart-item-quantity-display").val(newQuantity);
        
        
       let ranNo = $(this).data('random-no');
//        let sameEle = $(`[data-charge-product][data-random-no='${ranNo}']`).closest('tr');
         let sameEle = $('[data-charge-product][data-random-no="'+ranNo+'"]').closest('tr');
       const key = $(this).data('key');
       var data = "updates["+key+"]=0";
       $(sameEle).find('.cart-item-quantity-display').val(newQuantity);
        
        if(newQuantity == 0){
          updateMainandAddon(data,ranNo);
        }
      })
    }
    else if($('[data-template]').data('template') === 'product'){
      let flag = null;
      var productSettings = JSON.parse($('[data-prd-settings]').text());
      
      if($('[data-add-custom-size]')[0].checked == false){
        //to set flag to 0 at load if cart redirection is on for generic size selction
        if(productSettings.cartRedirect){
          	if(flag == 1 || flag == null){
            	flag = 0;
          	}
          }
        $('[data_line_item]').addClass('opacity-30')
        $('.cstm_size input').prop("disabled", true);
        $('[data-get-prd]').prop("disabled", true);
      }
      
      // code to change value of flag to stop repeated submission
      $('[data-add-custom-size]').change(function(e){
        var oldPrice = parseInt($('[data-new-price]').text().split('$')[1])
          if(flag == 1){
            flag = 0;
          }
         if($(this)[0].checked){
           	var width1 = $($('[data-custom-width]').find('input')[0]).val()
            var width2 = $($('[data-custom-width]').find('input')[1]).val()
            var height1 = $($('[data-custom-height]').find('input')[0]).val()
            var height2 = $($('[data-custom-height]').find('input')[1]).val()
           if(width1 >=0 && width2 >=0 && height1 >=0 && height2 >=0 ){
             if($('[data-door-addon]').length && oldPrice - ($('[data-product-select]').children('option:selected').data('price')/100) == 0){
				oldPrice = oldPrice + 59;
             }
             else if($('[data-window-addon]').length && oldPrice - ($('[data-product-select]').children('option:selected').data('price')/100) == 0){
             	oldPrice = oldPrice + 39;
             }
             $('[data-new-price]').text('$'+oldPrice+'.00');
           }
          $('[data_line_item]').removeClass('opacity-30').addClass('opacity-100')
          $('.cstm_size input').prop("disabled", false);
           $('[data-get-prd]').prop("disabled", false);

        }
        else{ // not checked 
          
          if($('[data-door-addon]').length && oldPrice > ($('[data-org-prc]').data('org-prc')/100)){
          	oldPrice = oldPrice - 59;
          }else if($('[data-window-addon]').length && oldPrice > ($('[data-org-prc]').data('org-prc')/100)){
          	oldPrice = oldPrice - 39;
          }
          $('[data-new-price]').text('$'+oldPrice+'.00');
         
          $('[data_line_item]').removeClass('opacity-100').addClass('opacity-30')
          $('.cstm_size input').prop("disabled", true);
          $('[data-get-prd]').prop("disabled", true);
        }
      });
      
      
      //select desired size on chnage of input
      $('[data-custom-size]').on('input',function(){
        if(flag == 1 || flag == null){
          flag = 0;
        }
       	var ele_width1 = $($('[data-custom-width]').find('input')[0]).val()
        var ele_width2 = $($('[data-custom-width]').find('input')[1]).val()
        var ele_height1 = $($('[data-custom-height]').find('input')[0]).val()
        var ele_height2 = $($('[data-custom-height]').find('input')[1]).val()
        const maxWidth = Math.max(ele_width1,ele_width2);
        const maxHeight = Math.max(ele_height1,ele_height2);
        let strsize = null;
        $('[data-width]').val(maxWidth.toString())
        $('[data-height]').val(maxHeight.toString())
        if($('[data-door-addon]').length){

          if(maxWidth <= 1200 && maxHeight <=2100){
            strsize = 'Single - up to 1200mmW/2100mmH';
          }
          else if( maxWidth <= 1800 && maxHeight <=2100){
            strsize = 'Double - up to 1800mmW/2100mmH';
          }
          else if( maxWidth <= 2300 && maxHeight <=2400){
            strsize = 'Large Double - up to 2100mmW/2400mmH';
          }
        }
        else if($('[data-window-addon]').length){
          if(maxWidth <= 1500 && maxHeight <=1500){
            strsize = 'up to 1500mmW/1500mmH';
          }
        }
        // select the required size and trigger change
        if($('[data-radio]').length){
          $('.data-Size input[value="'+strsize+'"]').prop('checked',true).trigger('change');
        }else if($('[data-select]').length){ // for dropdowns
          $('.data-Size option[value="'+strsize+'"]').prop('selected',true).trigger('change');
        }
      
      });
      
      
      
      
      // to keep the changed price nostant even if the swatch changes
      $('[data-product-select]').change(function(){
        if($('[data-add-custom-size]')[0].checked == true){
         var originalPrice = parseFloat((($(this).children('option:selected').data('price'))/100).toFixed(2));
         var newPrice = 0;
            if($('[data-door-addon]').length){
              newPrice = originalPrice + 59.00;
              setTimeout(function(){
                $('[data-new-price]').text('$'+newPrice+'.00');
              },50)
            }else if($('[data-window-addon]').length){
              newPrice = originalPrice + 39.00;
              setTimeout(function(){
                $('[data-new-price]').text('$'+newPrice+'.00');
              },50)
            }
        }
      })
      
      $('[data-product-form]').submit(function(e) {
//         console.log(flag)
        if(flag){
        	return;
        }
        e.preventDefault();
        
        if($('[data-product-form]')[0].checkValidity() == false){
         	return false;
        }else{
          if($('[data-add-custom-size]')[0].checked == true){
            
            // code for custom
            // code to add product when custom sizes are entered
            if($('[data-door-addon]').length || $('[data-window-addon]').length){
                if($('[data-add-custom-size]')[0].checked == false){
                  if($('[data-random-prop]').length){
                    $('[data-random-prop]').remove();
                  }
                }else{
                  if($('[data-random-prop]').length){
                    $('[data-random-prop]').remove();
                  }
                }
                const prdRandomId = Math.random();
  //               $('[data-product-form]').append(`<input type="hidden" data-random-prop value="${prdRandomId}" name="properties[randomId]">`)
                  $('[data-product-form]').append('<input type="hidden" data-random-prop value="'+prdRandomId+'" name="properties[randomId]">')

				// add to cart for doors
                if($('[data-door-addon]').length ){
                  const varId = $('[data-door-addon]').val();
                  const qty = $('input[name="quantity"]').val();
                  addAddonstoCart(varId,qty);
                }
                else if($('[data-window-addon]').length ){  // add to cart for windows
                  const varId = $('[data-window-addon]').val();
                  const qty = $('input[name="quantity"]').val();
                  addAddonstoCart(varId,qty);
                }

                function addAddonstoCart(varId,qty){
                  setTimeout(function() {
                    $.ajax({
                      type: 'POST', 
                      url: '/cart/add.js',
                      dataType: 'json', 
                      data: {id:varId,quantity:qty,properties:{'randomId': prdRandomId,'charge Product':true}},
                      success: function(data){
                        Shopify.getCart(function(cart) {
                          return $('.cart-link .cart-count').text(cart.item_count);
                        });
                      },
                      error: function(er){
                      },
                    });
                  },2000)
                }
            }
          }
          else{
            // code for diy
               if($('[data-random-prop]').length){
                 $('[data-random-prop]').remove();
               }
          }
         
          if(productSettings.cartRedirect){
           setTimeout(function(){
             flag = 1;
            $('[data-product-form]').submit();
           },3000)
          }else{
            flag = 1;
           $('[data-product-form]').submit();
          }
		return true;
        }
      });
      
      // color swatch code
      $('.swatch :radio').change(function() {
        var optionIndex = $(this).closest('.swatch').attr('data-option-index');
        var optionValue = $(this).val();
        $(this)
          .closest('form')
          .find('.single-option-selector')
          .eq(optionIndex)
          .val(optionValue)
          .trigger('change');
      });
    }
  }
  function updateMainandAddon(data,ranNo){
  	  $.ajax({
          type: 'POST',
          url: '/cart/update.js',
          dataType: 'json',
          data: data,
          success: function(res){
            $.each(res.items,function(i,ele){
              if(ele.properties.randomId == ranNo ){
                var dataAdd = "updates["+ele.key+"]=0";
                 $.ajax({
                  type: 'POST',
                  url: '/cart/update.js',
                  dataType: 'json',
                   data: dataAdd,
                   success: function(res){
                      setTimeout(function(){
                        location.reload();
                      },1000)
                   }
                 
                 });
              }
            
            })
           
          }
        });
  }
  
});





